window.addEventListener('load', function () {

    let currentSlide = 0,
        countElems = document.getElementsByClassName('testimonials__slider')[0].childElementCount,
        step = getComputedStyle(document.getElementsByClassName('testimonials__slider')[0]).width,
        mr = getComputedStyle(document.getElementsByClassName('testimonials__slider-slide')[0]).marginRight,
        ml = getComputedStyle(document.getElementsByClassName('testimonials__slider-slide')[0]).marginLeft,
        elemW = getComputedStyle(document.getElementsByClassName('testimonials__slider-slide')[0]).width;

    async function resetButton() {
        if (currentSlide === 0) {
            document.getElementsByClassName('js_previous')[0].classList.remove("testimonials__slider-previous_enabled");
            document.getElementsByClassName('js_previous')[0].classList.add("testimonials__slider-previous_disabled");
        } else {
            document.getElementsByClassName('js_previous')[0].classList.add("testimonials__slider-previous_enabled");
            document.getElementsByClassName('js_previous')[0].classList.remove("testimonials__slider-previous_disabled");
        }
        if ((currentSlide) * (step) + step >= wind) {
            document.getElementsByClassName('js_next')[0].classList.remove("testimonials__slider-next_enabled");
            document.getElementsByClassName('js_next')[0].classList.add("testimonials__slider-next_disabled");
        } else {
            document.getElementsByClassName('js_next')[0].classList.add("testimonials__slider-next_enabled");
            document.getElementsByClassName('js_next')[0].classList.remove("testimonials__slider-next_disabled");
        }
    }

    async function next() {
        if ((currentSlide) * (step) + step < wind) {
            document.getElementsByClassName('testimonials__slider-point_enabled')[0].className = 'testimonials__slider-point_disabled';
            currentSlide++;
            document.getElementsByClassName('testimonials__slider-point_disabled')[currentSlide].className = 'testimonials__slider-point_enabled';
            document.getElementsByClassName('testimonials__slider')[0].style.transform = 'translateX(-' + (currentSlide) * (step) + 'px)';
            resetButton();
        }
    }

    async function previous() {
        if (currentSlide !== 0) {
            document.getElementsByClassName('testimonials__slider-point_enabled')[0].className = 'testimonials__slider-point_disabled';
            currentSlide--;
            document.getElementsByClassName('testimonials__slider-point_disabled')[currentSlide].className = 'testimonials__slider-point_enabled';
            document.getElementsByClassName('testimonials__slider')[0].style.transform = 'translateX(-' + (currentSlide) * (step) + 'px)';
            resetButton();
        }
    }


    function move(event) {
        if (x < event.x) {
            previous();
        } else if (x > event.x) {
            next();
        }
    }

    async function swipe() {
        document.getElementsByClassName('testimonials__slider-window_js')[0].addEventListener('touchstart', handleTouchStart, false);
        document.getElementsByClassName('testimonials__slider-window_js')[0].addEventListener('touchmove', handleTouchMove, false);

        var xDown = null;
        var yDown = null;

        function getTouches(evt) {
            return evt.touches;
        }

        function handleTouchStart(evt) {
            const firstTouch = getTouches(evt)[0];
            xDown = firstTouch.clientX;
            yDown = firstTouch.clientY;
        };

        function handleTouchMove(evt) {
            if (!xDown || !yDown) {
                return;
            }

            var xUp = evt.touches[0].clientX;
            var yUp = evt.touches[0].clientY;

            var xDiff = xDown - xUp;
            var yDiff = yDown - yUp;

            if (Math.abs(xDiff) > Math.abs(yDiff)) {
                if (xDiff > 0) {
                    next();
                } else {
                    previous();
                }
                xDown = null;
                yDown = null;
            };
        }
    }
    swipe();
    elemW = elemW.substr(0, elemW.indexOf('px'));
    mr = mr.substr(0, mr.indexOf('px'));
    ml = ml.substr(0, ml.indexOf('px'));
    step = Number(step.substr(0, step.indexOf('px'))) + Number(mr);
    let wind = countElems * (Number(ml) + Number(elemW) + Number(mr));

    for (let i = 0; i < Math.ceil(wind / (Number(step))); i++) {
        let div = document.createElement('div');
        div.className = 'testimonials__slider-point_disabled';
        div.addEventListener('click', function (event) {
            document.getElementsByClassName('testimonials__slider-point_enabled')[0].className = 'testimonials__slider-point_disabled';
            currentSlide = i;
            document.getElementsByClassName('testimonials__slider-point_disabled')[currentSlide].className = 'testimonials__slider-point_enabled';
            document.getElementsByClassName('testimonials__slider')[0].style.transform = 'translateX(-' + (currentSlide) * (step) + 'px)';
            resetButton();
        })
        document.getElementsByClassName('js_points')[0].appendChild(div);
    }
    document.getElementsByClassName('testimonials__slider-point_disabled')[currentSlide].className = 'testimonials__slider-point_enabled';


    document.getElementsByClassName('js_previous')[0].addEventListener('click', previous);
    document.getElementsByClassName('js_next')[0].addEventListener('click', next);
});