window.addEventListener('load', function () {
  function checked() {
    document.getElementsByClassName('header-menu_js')[0].classList.toggle('header-menu_active');
    document.getElementsByClassName('header__checkbox_js')[0].classList.toggle('header__checkbox_active');
    for (let i = 0; i < 3; i++) {
      document.getElementsByClassName('header__line_js')[i].classList.toggle('header__line_active');
      document.getElementsByClassName('header__line_js')[1].classList.toggle('header__line_2');
    }
  }
  document.getElementsByClassName('header__checkbox_js')[0].addEventListener('click', checked);
});