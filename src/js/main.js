
function showMoreButton(element, maxSize, callback) {
    var fullStr = element.innerHTML,
        check = false;

    if (element.innerHTML.length > maxSize) {
        var shortText = element.innerHTML.slice(0, maxSize) + '...';
        element.innerHTML = shortText;
        callback(true);
    } else {
        callback(false);
    }

    var h = element.offsetHeight;

    function showMore(elem) {
        var repeat = 1, offsetHeight = elem.offsetHeight, lastOffsetHeight = null;
        var timer = setInterval(function () {
            if (!draw(repeat++)) {
                clearInterval(timer);
                return;
            }
            draw(repeat);
        }, 10);
        function draw(repeat) {
            elem.style.maxHeight = (repeat * 2) + h + 'px';
            if (!(repeat % 5)) {
                if (elem.offsetHeight != lastOffsetHeight) {
                    lastOffsetHeight = offsetHeight;
                    offsetHeight = elem.offsetHeight;
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    function moveBack(max, callback) {
        var repeat = 0;
        var timer = setInterval(function () {
            if (!draw(repeat++)) {
                callback();
                clearInterval(timer);
                return;
            }
        }, 10);
        function draw(repeat) {
            if (Number(max) - (repeat * 2) >= h) {
                element.style.maxHeight = Number(max) - (repeat * 2) + 'px';
                return true;
            } else {
                return false;
            }
        }
    }
    return function showMoreB() {
        check = !check;
        if (check) {
            element.innerHTML = fullStr;
            showMore(element);
        } else {
            max = element.offsetHeight;
            moveBack(max, function () {
                element.innerHTML = shortText;
            });
        }
    }
}

window.addEventListener('load', function () {
    

    // Testimonials


    var sizeCharOfSlide = 0;
    if (document.body.clientWidth < 500) {
        sizeCharOfSlide = 230;
    } else {
        sizeCharOfSlide = 220;
    }
    var countSliders = document.getElementsByClassName('testimonials__slider')[0].childElementCount;
    var viewMoreButton = null;
    for (var c = 0; c < countSliders; c++) {
        viewMoreButton = document.getElementsByClassName('testimonials__slide-show-more_js')[c];
        viewMoreButton.addEventListener('click', showMoreButton(
            document.getElementsByClassName('testimonials__text_js')[c],
            sizeCharOfSlide,
            function (view) {
                if (!view) {
                    viewMoreButton.classList.add('testimonials__slide-show-more_none');
                }
            }
        ))
    }
});